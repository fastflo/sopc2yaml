#!/usr/bin/python
"""
 Copyright 2016 Florian Schmidt

 This file is part of sopc2yaml

 sopc2yaml is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 sopc2yaml is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with robotkernel.  If not, see <http://www.gnu.org/licenses/>.
"""

import pprint

from lxml import etree
import yaml

class sopc_reader(object):
    def __init__(self):
        pass
    def as_dict(self, parent):
        d = {}
        for tag in parent:
            v = tag.text
            if v is not None and v.isdigit():
                v = int(v)
            d[tag.tag] = v
        return d
    def get_childs(self, parent):
        childs = {}
        for tag in parent:
            if "name" in tag.attrib:
                gname = tag.tag + "s"
                l = childs.get(gname)
                if l is None:
                    l = childs[gname] = {}
                d = l[tag.attrib["name"]] = {}
                attrs = dict(tag.attrib)
                del attrs["name"]
                if attrs:
                    d["attributes"] = attrs
                tchilds = self.get_childs(tag)
                if tchilds:
                    d["childs"] = tchilds
                continue
            if tag.tag == "assignment":
                a = childs.get("assignments")
                if a is None:
                    a = childs["assignments"] = {}
                d = self.as_dict(tag)
                v = d["value"]
                a[d["name"]] = v
                continue
            if len(tag):
                what = tag.tag + "s"
                a = childs.get(what)
                if a is None:
                    a = childs[what] = []
                childs[what].append(self.as_dict(tag))
                continue
            if isinstance(tag.tag, (str, unicode)):
                if tag.text in ("true", "false"):
                    childs[tag.tag] = tag.text == "true"
                else:
                    childs[tag.tag] = tag.text
            #import pdb; pdb.set_trace()            
        return childs
    def read(self, fn):
        self.tree = etree.parse(fn)
        root = self.tree.getroot()
        self.ereport = {}
        for tag in root:
            if "name" in tag.attrib:
                gname = tag.tag + "s"
                l = self.ereport.get(gname)
                if l is None:
                    l = self.ereport[gname] = {}
                d = l[tag.attrib["name"]] = {}
                attrs = dict(tag.attrib)
                del attrs["name"]
                if attrs:
                    d["attributes"] = attrs
                childs = self.get_childs(tag)
                if childs:
                    d["childs"] = childs
            else:
                print "tag without name: %s" % tag
    def export(self, fn):
        #del self.ereport["connections"]
        if False:
            fp = file(fn + ".py", "wb")
            fp.write(pprint.pformat(self.ereport))
            fp.close()
                 
        fp = file(fn, "wb")
        yaml.dump(self.ereport, fp, default_flow_style=False)
        

if __name__ == "__main__":
    r = sopc_reader()
    r.read("soc_system.sopcinfo")
    r.export("soc_system.yaml")
