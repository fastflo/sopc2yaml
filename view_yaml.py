#!/usr/bin/python -u
"""
 Copyright 2016 Florian Schmidt

 This file is part of sopc2yaml

 sopc2yaml is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 sopc2yaml is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with robotkernel.  If not, see <http://www.gnu.org/licenses/>.
"""

import sys
import gtk
import gobject
import yaml

try:
    from collections import OrderedDict
except:
    from OrderedDict import OrderedDict


def get_ordered_dict_from_yaml(source):
    class OrderedLoader(yaml.Loader):
        pass
    def construct_mapping(loader, node):
        loader.flatten_mapping(node)
        return OrderedDict(loader.construct_pairs(node))
    OrderedLoader.add_constructor(
        yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG, construct_mapping)
    try:
        return yaml.load(source, OrderedLoader)
    except:
        print "failed to parse this yaml:\n%s\n%s" % (source, sys.exc_value)
        raise
    #return OrderedDict(yaml.load('!omap\n%s' % source))

def format_value(context, value):
    #print "format %r in %r" % (value, context)
    if not context:
        return str(value)
    last = context # context[-1]
    if context == "ptr" or context.endswith("_ptr") or context.endswith("_ptrs"):
        return "0x%08x" % value
    return str(value)

single_col_state_display = True


class yaml_viewer(object):
    def __init__(self, fn):
        print "read...", fn
        fp = file(fn, "rb")
        self.data = yaml.load(fp)
        fp.close()
        print "build gui..."
        ##
        self.w = gtk.Window()
        self.w.connect("delete-event", lambda *args: gtk.main_quit())
        self.w.set_default_size(500, 500)
        self.sw = gtk.ScrolledWindow()
        self.w.add(self.sw)
        ##
        if single_col_state_display:
            m = gtk.TreeStore(gobject.TYPE_STRING)
        else:
            m = gtk.TreeStore(gobject.TYPE_STRING, gobject.TYPE_STRING)
        self.state_model = m
        tv = self.state_tv = gtk.TreeView()
        self.sw.add(tv)
        ##
        tv.set_model(m)
        tv.insert_column_with_attributes(-1, "what", gtk.CellRendererText(), text=0)
        if not single_col_state_display:
            cr = gtk.CellRendererText()
            cr.set_property("editable", True)
            tv.insert_column_with_attributes(-1, "value", cr, text=1)
        ###
        def append_value(parent, value, key_prefix=None, context=[]):
            is_map = type(value) in (OrderedDict, dict)
            is_seq = type(value) in (tuple, list)
            is_scalar = not is_map and not is_seq
            
            if not key_prefix:
                key_prefix = ""
            elif single_col_state_display:
                key_prefix += ": "
            if is_scalar:
                if single_col_state_display:
                    m.append(parent, ("%s%s" % (key_prefix, format_value(context, value)), ))
                else:
                    m.append(parent, (key_prefix, format_value(context, value), ))
                return
            if is_map:
                if not single_col_state_display:
                    child = m.append(parent, (key_prefix, ""))
                else:
                    child = m.append(parent, (key_prefix, ))
                update_mapping(child, value, context=context)
                return
            # has to be a list
            if not single_col_state_display:
                child = m.append(parent, (key_prefix, ""))
            else:
                child = m.append(parent, (key_prefix, ))
            for item in value:
                append_value(child, item, context=context)

        def update_mapping(parent, state, context):
            # state is a map!
            for key, value in state.iteritems():
                #new_context = list(context)
                #new_context.append(key)
                append_value(parent, value, key, context=key)
                    
        context = []
        m.clear()
        iter = m.get_iter_root()
        update_mapping(iter, self.data, context)
        self.state_tv.expand_all()
        self.debug_info_up_to_date = True
        self.w.show_all()
        
    def run(self):
        print "ready"
        gtk.main()
        
if __name__ == "__main__":
    v = yaml_viewer(sys.argv[1])
    v.run()
    
